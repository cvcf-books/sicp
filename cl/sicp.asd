(defsystem #:sicp
  :description "Code from Structure and Interpretation of Computer Programs"
  :version "1.0.0"
  :author "Cameron V Chaparro <cameron@cameronchaparro.com>"
  :license "GPL3"
  :components ((:module "01"
                :components ((:module "examples"
                              :components ((:file "package")
                                           (:file "sqrt")
                                           (:file "count-change")))
                             (:module "exercises"
                              :components ((:file "01")
                                           (:file "02")
                                           (:file "03")
                                           (:file "04")
                                           (:static-file "05.md")
                                           (:static-file "06.md")
                                           (:file "07")
                                           (:file "08")
                                           (:file "09")
                                           (:file "10")
                                           (:file "11")
                                           (:file "12")
                                           (:static-file "13.md")))))))
