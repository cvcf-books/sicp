# SICP

Example code and my implementations of exercises from the book Structure and
Interpretation of Computer Programs, 2nd edition. Note that while SICP code is
implemented using Scheme, the implementations here use Common Lisp, instead.

## Requirements

All the solutions here were written using, and are tested with, SBCL version
2.0.11.  Other implemenations may work, but haven't been tested.

## Author

Cameron V Chaparro <cameron@cameronchaparro.com>

## License

Copyright (c) 2020 Cameron V Chaparro

Note that *only* my personal implementations of exercises, etc. in this
repository are Copyright to me. All examples, for example, remain with their
respective copyright holders.
