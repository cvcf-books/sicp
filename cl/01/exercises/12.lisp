;;;;
;;
;; The following pattern of numbers is called Pascal's triangle.
;;
;;           1
;;         1   1
;;       1   2   1
;;     1   3   3   1
;;   1   4   6   4   1
;;          ...
;;
;; The numbers at the edge of the triangle are all 1, and each number inside the
;; triangle is the sum of the two numbers above it.  Write a procedure that
;; computes elements of Pascal's triangle by means of a recursive process.
;;
;; License: GPL3

(defpackage #:sicp.exercises.01.12
  (:use #:cl))

(in-package #:sicp.exercises.01.12)

(defun pascals-triangle (rows)
  (loop :for row :from 1 :upto rows
        :do (format t "~{~a ~^~}~%" (pascals-triangle-row row))))

(defun pascals-triangle-row (n)
  (cond ((= n 1) '(1))
        ((= n 2) '(1 1))
        (t (append '(1) (add-pairs (pascals-triangle-row (1- n))) '(1)))))

(defun add-pairs (row)
  (let ((row-size (length row)))
    (if (= row-size 2)
        (list (apply #'+ row))
        (append (list (+ (first row) (second row)))
                (add-pairs (rest row))))))
