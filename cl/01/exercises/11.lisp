;;;;
;;
;; A function F is defined by the rule that:
;;   f(n) = n                                  if n < 3 and
;;   f(n) = f(n - 1) + 2 f(n - 2) + 3 f(n - 3) if n >= 3.
;; Write a procedure that computes F by means of a recursive process.  Write a
;; procedure that computes F by means of an iterative process.
;;
;; License: GPL3

(defpackage #:sicp.exercises.01.11
  (:use #:cl))

(in-package #:sicp.exercises.01.11)

(defun f1 (n)
  "The recursive version of F as defined above."
  (if (< n 3)
      n
      (+ (f1 (- n 1))
         (* 2 (f1 (- n 2)))
         (* 3 (f1 (- n 3))))))

(defun f2 (n)
  "The iterative version of F as defined above."
  ;; Admittedly, I just followed the previous pattern for this recursive part.
  ;; I'm not completely sure of the details of why it works (assuming it does,
  ;; of course, but I checked that we get the same results for values from 0 to
  ;; 30) but, at a higher level, I think it's because F2-ITER is essentially
  ;; "counting up" (i.e. counting from 1 to N) as opposed to "counting down"
  ;; (i.e. counting from N down to 1) so we avoid repeatedly calculating values
  ;; we've already calculated.
  ;;
  ;; For example, using the recursive process above, how many times would we end
  ;; up calculating (f1 4) -> 11 when evaluating (f1 10)?  The answer: at least
  ;; 24 (based on my count).  When compared to evaluating (f2 10), we'd only
  ;; calculate (f2 4) -> 11 - and, for that matter, (f2 x) where x < 10 - once.
  (labels ((f2-iter (a b c count)
             (if (zerop count)
                 c
                 (f2-iter (+ a (* 2 b) (* 3 c)) a b (1- count)))))
    (f2-iter 2 1 0 n)))
