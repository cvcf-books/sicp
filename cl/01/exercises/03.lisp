;;;;
;;
;; Define a procedure that takes three numbers as arguments and returns the sum
;; of the squares of the two larger numbers.
;;
;; License: GPL3

(defpackage #:sicp.exercises.01.03
  (:use #:cl))

(in-package #:sicp.exercises.01.03)

(defun sum-of-squares-of-largest (x y z)
  (let* ((numbers (list x y z))
         (a (apply #'max numbers))
         (b (apply #'max (set-difference numbers (list a)))))
    (+ (* a a) (* b b))))
