;;;;
;;
;; The following procedure copmutes a mathematical function called Ackermann's
;; function.
;;
;;   (defun A (x y)
;;     (cond ((= y 0) 0)
;;           ((= x 0) (* 2 y))
;;           ((= y 1) 2)
;;           (t (A (1- x)
;;                 (A x (1- y))))))
;;
;; What are the values of following expressions?
;;
;; (A 1 10)
;;
;; (A 2 4)
;;
;; (A 3 3)
;;
;; Consider the following procedures where A is the procedure defined above:
;;
;;   (defun f (n) (A 0 n))
;;
;;   (defun g (n) (A 1 n))
;;
;;   (defun h (n) (A 2 n))
;;
;;   (defun k (n) (* 5 n n))
;;
;; Give concise mathematical definitions for the functions computed by the
;; procedures F, G, and H for positive integer values of N.  For example, (K n)
;; computes 5n^2.
;;
;; License: GPL3

(defpackage #:sicp.exercises.01.10
  (:use #:cl))

(in-package #:sicp.exercises.01.10)

;;
;; (A 1 10)
;; -> (A 0 (A 1 9))
;; -> (A 0 (A 0 (A 1 8)))
;; -> (A 0 (A 0 (A 0 (A 1 7))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 1 6)))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 5))))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 4)))))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 3))))))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 2)))))))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 1))))))))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 2)))))))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 4))))))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 8)))))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 16))))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 0 32)))))
;; -> (A 0 (A 0 (A 0 (A 0 64))))
;; -> (A 0 (A 0 (A 0 128)))
;; -> (A 0 (A 0 256))
;; -> (A 0 512)
;; => 1024
;;
;; (A 2 4)
;; -> (A 1 (A 2 3))
;; -> (A 1 (A 1 (A 2 2)))
;; -> (A 1 (A 1 (A 1 (A 2 1))))
;; -> (A 1 (A 1 (A 1 2)))
;; -> (A 1 (A 1 (A 0 (A 0 1))))
;; -> (A 1 (A 1 (A 0 2)))
;; -> (A 1 (A 1 4))
;; -> (A 1 (A 0 (A 1 3)))
;; -> (A 1 (A 0 (A 0 (A 1 2))))
;; -> (A 1 (A 0 (A 0 (A 0 (A 1 1)))))
;; -> (A 1 (A 0 (A 0 (A 0 2))))
;; -> (A 1 (A 0 (A 0 4)))
;; -> (A 1 (A 0 8))
;; -> (A 1 16)
;; -> (A 0 (A 1 15))
;; -> (A 0 (A 0 (A 1 14)))
;; -> (A 0 (A 0 (A 0 (A 1 13))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 1 12)))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 11))))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 10)))))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 10)))))))
;; For simplicity sake, we'll take the value from above and use it here.
;;                                  ||||
;;                                  vvvv
;; -> (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 1024))))))
;; -> (A 0 (A 0 (A 0 (A 0 (A 0 2048)))))
;; -> (A 0 (A 0 (A 0 (A 0 4096))))
;; -> (A 0 (A 0 (A 0 8192)))
;; -> (A 0 (A 0 16384))
;; -> (A 0 32768)
;; => 65536
;;
;; (A 3 3)
;; -> (A 2 (A 3 2))
;; -> (A 2 (A 2 (A 3 1)))
;; -> (A 2 (A 2 2))
;; -> (A 2 (A 1 (A 2 1)))
;; -> (A 2 (A 1 2))
;; -> (A 2 (A 0 (A 1 1)))
;; -> (A 2 (A 0 2))
;; -> (A 2 4)
;; Again, for simplicity sake, we'll take the value fro mabove and use it here.
;;    |||||
;;    vvvvv
;; => 65536
;;

(defun a (x y)
  (cond ((= y 0) 0)
        ((= x 0) (* 2 y))
        ((= y 1) 2)
        (t (a (1- x)
              (a x (1- y))))))

(a 1 10)                                ; => 1024
(a 2 4)                                 ; => 65536
(a 3 3)                                 ; => 65536

;;
;; (F n) computes (A 0 n) which computes 2n
;;
;; (G n) computes (A 1 n) which computes 2^n
;;
;; (H n) computes (A 2 n) which computes:
;;  if n = 0,  0;
;;  if n = 1,  2;
;;  otherwise, 2^(H (1- n))
;;
