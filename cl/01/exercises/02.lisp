;;;;
;;
;; Translate the following expression into prefix form:
;;
;;    5 + 4 + (2 - (3 - (6 + 5/4)))
;;    -----------------------------
;;           3(6 - 2)(2 - 7)
;;
;; License: GPL3

(/ (+ 5 4 (- 2 (- 3 (+ 6 5/4))))
   (* 3 (- 6 2) (- 2 7)))
