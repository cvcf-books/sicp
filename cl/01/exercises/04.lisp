;;;;
;;
;; Observe that our model of evaluation allows for combinations whose operators
;; are compound expressions.  Use this observation to describe the behavior of
;; the following procedure:
;;
;;   (defun a-plus-abs-b (a b)
;;     ((if (> b 0) #'+ #'-) a b))
;;
;; License: GPL3

(defpackage #:sicp.exercises.01.04
  (:use #:cl))

(in-package #:sicp.exercises.01.04)

;; This function will, first, determine if the second parameter is positive and,
;; if so, it will apply the #'+ function to the parameters A and B.  If it is not
;; positive, then it will apply the #'- function to the parameters A and B.
(defun a-plus-abs-b (a b)
  "CL version of A-PLUS-ABS-B since CL does not seem to allow using this kind of
form as the function name (or, at least I don't know how to do it)."
  (funcall (if (> b 0) #'+ #'-) a b))
