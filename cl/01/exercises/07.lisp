;;;;
;;
;; The GOOD-ENOUGH? test used in computing square roots will not be very
;; effective for finding the square roots of very small numbers.  Also, in real
;; computers, arithmetic operations are almost always performed with limited
;; precision.  This makes our test inadequate for very large numbers.  Explain
;; these statements, with examples showing how the test fails for small and
;; large numbers.  An alternative strategy for implementing GOOD-ENOUGH? is to
;; watch how GUESS changes from one iteration to the next and to stop when
;; change is a very small fraction of the guess.  Design a square root procedure
;; that uses this kind of end test.  Does this work better for small and large
;; numbers?
;;
;; License: GPL3

(defpackage #:sicp.exercises.01.07
  (:use #:cl))

(in-package #:sicp.exercises.01.07)

;;
;; For very small numbers, such as 0.0004 (which is, of course, smaller than the
;; current threshold of 0.001), GOOD-ENOUGH? would not be good enough (pun
;; intended) since the threshold is larger than the value itself.  This causes
;; GOOD-ENOUGH? to return T before we've found an actual square root.  In the
;; above example:
;;
;;   (sqrt* 0.0004) -> 0.03540088
;;
;; where:
;;
;;   (square 0.03540088) -> 0.0012674226 (which obviously doesn't equal 0.0004).
;;
;;
;; For very large numbers, such as 123456789123456789, we run into a problem
;; with infinite recursion due to the limited precision when doing arithmetic on
;; computers.  As it turns out, for numbers such as this, the guess ends up
;; being larger than the threshold when we've reached an answer for the square
;; root due to integer wrap-around.  In the above example:
;;
;;   (sqrt* 123456789123456789)
;;
;; recurses indefinitely even though it's found the expected value.  It cannot
;; terminate due to the following condition never actually evaluating to T and
;; it continually checks the same condition because it's gotten to the actual
;; square root:
;;
;;   (good-enough? 3.513642e8 123456789123456789)
;;   -> (< (abs (- (square 3.513642e8) 123456789123456789)) 0.001)
;;   -> (< (abs (- 1.234568e17 123456789123456789)) 0.001)
;;   -> (< (abs 8.589935e9) 0.001)
;;   -> (< 8.589935e9 0.001)
;;   => NIL
;;
;;
;; Thus, we see that the current implementation of GOOD-ENOUGH? is not
;; sufficient for very small or very large values since it stops too soon before
;; finding the actual root.
;;

(defun sqrt* (x)
  (sqrt-iter 1.0 x 0.0))

(defun sqrt-iter (guess x last-guess)
  (if (good-enough? guess last-guess)
      guess
      (sqrt-iter (improve guess x) x guess)))

(defun good-enough? (guess last-guess)
  (< (abs (- guess last-guess)) 0.000001))

(defun square (x)
  (* x x))

(defun improve (guess x)
  (average guess (/ x guess)))

(defun average (x y)
  (/ (+ x y) 2))
