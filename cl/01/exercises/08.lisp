;;;;
;;
;; Newton's method for cube roots is based on the fact that if y is an
;; approximation to the cube root of x, then a better approximation is given by:
;;
;;   x/y^2 + 2y
;;   ----------
;;        3
;;
;; Use this formula to implement a cube-root procedure analogous to the
;; square-root procedure.  (In section 1.3.4 we will see how to implement
;; Newton's method in general as an abstraction of these cube-root and
;; square-root procedures.)
;;
;; License: GPL3

(defpackage #:sicp.exercises.01.08
  (:use #:cl))

(in-package #:sicp.exercises.01.08)

(defun cbrt (x)
  (cbrt-iter 1.0 x 0.0))

(defun cbrt-iter (guess x last-guess)
  (if (good-enough? guess last-guess)
      guess
      (cbrt-iter (improve guess x) x guess)))

(defun good-enough? (guess last-guess)
  (< (abs (- guess last-guess)) 0.000001))

(defun improve (guess x)
  (average* (/ x (square guess)) (* 2 guess)))

(defun cube (x)
  (* (square x) x))

(defun square (x)
  (* x x))

(defun average* (x y)
  (/ (+ x y) 3))
